These spack packages are meant to be used in a "custom repo" that
takes precedence over the built-in repo. The package recipes have
been prepared by the Siesta Project, for Siesta itself and for
some of the libraries needed. Some third-party recipes (elpa, pexsi)
might also be available in the built-in repo, but the versions here
should be used instead (until the changes have been sent upstream
and accepted).

These recipes might need some tweaking in specific systems, despite
spack's claims to portability. This is sometimes due to misconfiguration
(e.g. the 'mpi' metapackage in Leonardo). Maintaining all those
tweaks in the Siesta repo is not workable, so we provide this kind
of information on specific systems in a separate repo:

  https://gitlab.com/siesta-project/ecosystem/build-tools.git

