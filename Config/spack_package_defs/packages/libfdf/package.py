from spack import *


class Libfdf(CMakePackage):
    """Flexible Data Format library."""

    homepage = "https://gitlab.com/siesta-project/libraries/libfdf"

    git = 'https://gitlab.com/siesta-project/libraries/libfdf.git'

    version("0.5.1", commit="206a3d6c4628")   # same as above, but trusted
    
    depends_on('cmake@3.17.0:', type='build')

    def cmake_args(self):
       args = []

       return args
